const express = require('express');
const cors = require('cors');
const path = require('path');
const mongoose = require('mongoose');

app = express();
app.use(cors());
const port = 3005;

const http = require('http').Server(app);

const io = require('socket.io');
const socket = io(http);

// DB Conn
const connect = require('./dbConnections');
const Chat = require('./models/ChatSchema');

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/public/index.html'));
});

app.get('/chat/:roomID', (req, res) => {
  let { roomID } = req.params;

  connect.then((db) => {
    Chat.find({ roomID: roomID }, (err, data) => {
      if (err) {
        res.status(500).json({ error: true, message: err.message });
      }
      res.json(data);
    });
  });
});

socket.on('connection', (socket) => {
  console.log('user connected');

  socket.on('send', (msg) => {
    console.log('User send message ...');

    //broadcast message to everyone in port:5000 except yourself.
    socket.broadcast.emit('received', msg);

    // save chat to the database
    connect.then((db) => {
      let chatMessage = new Chat({ message: msg.message, username: msg.username, roomID: msg.roomID });
      chatMessage.save();
    });
  });
});

http.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
